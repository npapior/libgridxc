#if defined HAVE_CONFIG_H
#include "config.h"
#endif

program test_ldaxc

  use fruit
  use ldaxc_basket

  implicit none

  call init_fruit()
  call init_fruit_xml()

  call fruit_basket()

  call fruit_summary()
  call fruit_summary_xml()
  call fruit_finalize()

end program test_ldaxc
